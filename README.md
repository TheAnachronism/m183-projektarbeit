# M183-Projektarbeit

## User Commits
Notice that even though most commits were made by Tobias (TheAnachronism), this doesn't mean Dzenis didn't to anything.
Most of the time we were pair-programming over discord and it just worked better with having Tobias share his screen.
This was done like this, because Dzenis wasn't too familiar with the client framework (Blazor) and this way it was easier and we were able to be more productive.

## Requirements
Docker has to be installed so the postgres database server can be used.
The build process of the server project is configured to make sure the postgres server inside docker is started with ```docker-compose up -d postgres```.

## Setup
To setup the development environment, please copy the docker-compose.template.yml file in the docker directory, rename it to docker-compose.yml and enter a correct password for the postgres server. Also copy the launchSettings.template.json in the Properties directory of the Server project, rename that to launchSettings.json and adjust the connection string there to use the previously entered password.

Also in the now created launchSettings.json file replace the [ApiKey] with the api key from the [sms gateway](https://m183.gibz-informatik.ch/sms-gateway).

### Database users
in launchsettings.json is an environment variable called ```ASPNETCORE_ENSURE_ADMINISTRATOR_USER_IS_CREATED```. It determines if it should create an administrator user if it doesn't exist yet. This is just for development/local purposes so no manual changes have to be made on the database.
If it has to create the user it will ask for the phone number in the console. A valid an accessible phone number has to be provided, otherwise it's impossible to login with that account.

## Database Migrations
Currently its configured the apply migrations automatically on startup. This can be disabled with the environment variable ASPNETCORE_APPLY_MIGRATIONS_ON_STARTUP.
If migrations want to be applied manually remember to set the environment variable with $env:ConnectionStrings:DefaultConnection=... with the used connection string.

#### dotnet cli
1. With the dotnet cli the [ef core tools](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli#install-the-tools) have to be installed.
2. Enter ```dotnet ef database update``` to update the database to the newest version or creating it if it doesn't exist yet.

##### Package Manager Console
1. Open the solution in Visual Studio and open the package manager console there.
2. Then set the previously mentioned connection string with $env:ConnectionStrings:DefaultConnection=...
3. Enter ```Update-Database``` and ef core will create the database or apply the necessary migrations.

### Specifc discussions with the stakeholder
1. After some discussion about the criteria about preventing url-tampering with the dashboards (/user/dashboard and /admin/dashboard) it was decided, that a universal route at just /dashboard should be created. The kind of posts there shown is then automatically decided on the backend and with that the possibility for those url-tampering attacks doesn't even exist.

## Logging
With the nuget package Serilog the default asp.net core logger is wrapped to better write to specific targets (called sinks in Serilog). The configuration is that it writes always into console,
Warning, Error and Fatals into a file called exceptions(date).log and Information and Debug into a information(date).log file. Technically the debug log level could be ignored because the information printed there is currently just the http requests the API makes, when sending a SMS over the gateway.
With Serilog and its already existing sinks (log targets) many more configurations are possible. If it should get necessary it could also write into a database or even send out emails.

## Password storage
Because the ASP.NET Identity package is used the password hasher from there gets also used. It uses the [PBKDF2 algorithm](https://tools.ietf.org/html/rfc2898#section-5.2). More about it [here](https://docs.microsoft.com/en-us/aspnet/core/security/data-protection/consumer-apis/password-hashing?view=aspnetcore-5.0).

## Used Libraries

- Client Project (Blazor WebAssembly)
    1. Blazorise.Bootstrap, A library containing Blazor Components for easier use, i.E. Modal
    2. Blazorise.Icons.FontAwesome, A library for the font awesome icons to use within Blazorise components
    3. Faso.Blazor.Spinkit, A library containing a few Blazor Components to show different kind of loading animations. Used to show a loading wheel when doing an API request
    4. Humanizer.Core, Helps format DateTime to a readable form, i.E. `15 minutes ago`
    5. Markdig, C# Markdown helper library
    6. Microsoft.AspNetCore.Components
        - .Authorization, The authorization package which helps to manage the authorization state on the client app
        - .WebAssembly, The web assembly library which blazor needs to run in the browser
        - .WebAssembly.DevServer, A Debug proxy for visual studio to debug blazor code
    7. Microsoft.AspNetCore.WebUtilities, A library to help with different web components (used to parse query strings in this case).
    8. Newtonsoft.Json, Serializer library for json
    9. QRCoder, Library to generate qr codes

- .Common Library
    1. Microsoft.AspNetCore.Http.Abstractions, Abstractions for the http package of asp.net core 
    2. Microsoft.AspNetCore.Identity.EntityFrameworkCore, Used to migrate the database, (type IdentityDbContext is required).
    3. Microsoft.Extensions.Configuration.Abstractions, Abstractions of the configuration package of C# (IConfiguration is required).
    4. Microsoft.Extensions.Hosting.Abstractions, (type IHost is required).
    5. Serilog, the library for the used logging framework.
        - Serilog.Enrichers.Environment, Serilog helper to add environment information to the logs.
        - Serilog.Expressions, Helper library to filter which logs should get written into which loggers.
        - Serilog.Sinks.Console, Library to write logs to the console.
        - Serilog.Sinks.File, Library to write logs to a file.
- Server Project
    1. Microsoft.AspNetCore.Components.WebAssembly.Server, Runtime server for the blazor application.
    2. Microsoft.AspNetCore.Identity.EntityFrameworkCore, the entity framework core implmentation for the asp.net core identity framework.
    3. Microsoft.AspNetCore.Mvc.NewtonsoftJson, json serializer package to use for the controllers.
    4. Microsoft.EntityFrameworkCore.Tools, Used to generate ef core migrations and update the database.
    5. Newtonsoft.Json, Json serializer library
    6. Npgsql.EntityFrameworkCore.PostgreSQL, Postgresql connection for ef core.
    7. Serilog, chosen logger library
        - Serilog.AspNetCore, the integration into the asp.net core framework of the serilog logger library
        - Serilog.Enrichers.Environment, Adds environment information to the logs.
        - Serilog.Expressions, Helper library to filter which logs should get written into which logger.s

## XSS-Attacks
Because all the content which can be entered by the user an all the parameters over the url are either not even directly loaded onto the page or are loaded with razor syntax. XSS is prevented there by default so we don't have to worry about that.