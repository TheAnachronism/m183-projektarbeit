﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace M183Project.Server.Migrations
{
    public partial class AddedNoteFieldToSmsTokens : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "SmsTokens",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Note",
                table: "SmsTokens");
        }
    }
}
