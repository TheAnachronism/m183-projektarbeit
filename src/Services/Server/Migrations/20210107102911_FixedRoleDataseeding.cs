﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace M183Project.Server.Migrations
{
    public partial class FixedRoleDataseeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ac79c7fa-914a-455b-bcc4-aee9d940f85f");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b615f257-2f72-4085-8f8d-d931d848802b");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "05e0af3a-e67e-4c10-97bf-d42930484e1a", "05e0af3a-e67e-4c10-97bf-d42930484e1a", "Administrator", "ADMINISTRATOR" },
                    { "d7be1a63-bd24-4bbc-9872-9be3d1f82568", "d7be1a63-bd24-4bbc-9872-9be3d1f82568", "User", "USER" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "05e0af3a-e67e-4c10-97bf-d42930484e1a");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d7be1a63-bd24-4bbc-9872-9be3d1f82568");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "ac79c7fa-914a-455b-bcc4-aee9d940f85f", "5f3fbfb6-1de5-41a2-abe8-b8494b83e2f4", "Administrator", null },
                    { "b615f257-2f72-4085-8f8d-d931d848802b", "b766e4b2-b281-4eab-8c2d-6df5ea7956ed", "User", null }
                });
        }
    }
}
