﻿using System;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using M183Project.Common.Post;
using M183Project.Common.Roles;
using M183Project.Server.Data;
using Microsoft.AspNetCore.Identity;

namespace M183Project.Server.Helpers
{
    public static class QueryFilterHelper
    {
        public static async Task<Expression<Func<Post, bool>>> GetPostFilterAsync(UserManager<ApplicationUser> userManager, ClaimsPrincipal userClaimsPrincipal)
        {
            var user = await userManager.GetUserAsync(userClaimsPrincipal);
            Expression<Func<Post, bool>> query;
            if (userClaimsPrincipal.Identity?.IsAuthenticated == false)
                query = x => x.Status == PostStatus.Published;
            else if (await userManager.IsInRoleAsync(user, DatabaseRoles.Administrator))
                query = x => true;
            else
                query = x => x.Status == PostStatus.Published || x.OriginalPoster == user;

            return query;
        }

        public static async Task<Expression<Func<Comment, bool>>> GetCommentFilterAsync(
            UserManager<ApplicationUser> userManager, ClaimsPrincipal userClaimsPrincipal)
        {
            var user = await userManager.GetUserAsync(userClaimsPrincipal);
            Expression<Func<Comment, bool>> query;

            if (userClaimsPrincipal.Identity?.IsAuthenticated == false)
                query = x => x.Post.Status == PostStatus.Published;
            else if (await userManager.IsInRoleAsync(user, DatabaseRoles.Administrator))
                query = x => true;
            else
                query = x => x.Post.Status == PostStatus.Published || x.Post.OriginalPoster == user;

            return query;
        }
    }
}
