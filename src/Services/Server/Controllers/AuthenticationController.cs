﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Threading.Tasks;
using M183Project.Common.Roles;
using M183Project.Server.Data;
using M183Project.Server.Services;
using M183Project.Shared.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace M183Project.Server.Controllers
{
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<AuthenticationController> _logger;
        private readonly SmsTokenService _smsTokenService;

        public AuthenticationController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager, ILogger<AuthenticationController> logger, SmsTokenService smsTokenService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _logger = logger;
            _smsTokenService = smsTokenService;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var user = await _userManager.FindByNameAsync(request.UserName);
            if (user == null)
            {
                _logger.LogInformation($"Login request with invalid username: {request.UserName}");
                return Unauthorized();
            }

            await _smsTokenService.CreateSmsToken(user);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> LoginSms([FromBody] SmsLoginRequest request)
        {
            var (userName, password, smsCode) = request;
            var user = await _userManager.FindByNameAsync(userName);
            var passwordResult = await _signInManager.CheckPasswordSignInAsync(user, password, false);
            var tokenResult = await _smsTokenService.VerifySmsToken(user, smsCode);
            if (!passwordResult.Succeeded || !tokenResult.IsSuccessful)
                return Unauthorized();

            await _signInManager.SignInAsync(user, true);

            await _smsTokenService.RemoveToken(tokenResult.Token);
            
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterRequest request)
        {
            var user = new ApplicationUser
            {
                Email = request.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = request.UserName,
                PhoneNumber = request.PhoneNumber
            };

            var result = await _userManager.CreateAsync(user, request.Password);
            if (!result.Succeeded)
            {
                _logger.LogWarning("Register request account creation failed.");
                return Unauthorized(result.Errors.FirstOrDefault()?.Description);
            }

            await _userManager.AddToRoleAsync(user, DatabaseRoles.User);
            await _signInManager.SignInAsync(user, true);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetCurrentUserInfo()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
                return Unauthorized();

            var userClaims = await _userManager.GetClaimsAsync(user);
            var claims = userClaims.Select(x => new KeyValuePair<string, string>(x.Type, x.Value)).ToList();

            var userRoles = await _userManager.GetRolesAsync(user);
            foreach (var userRole in userRoles)
            {
                claims.Add(new KeyValuePair<string, string>(ClaimTypes.Role, userRole));
                var role = await _roleManager.FindByNameAsync(userRole);
                if (role == null) continue;

                var roleClaims = await _roleManager.GetClaimsAsync(role);
                claims.AddRange(roleClaims.Select(x => new KeyValuePair<string, string>(x.Type, x.Value)));
            }

            return Ok(new CurrentUserResponse(User?.Identity?.IsAuthenticated ?? false, User?.Identity?.Name, claims));
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }
    }
}
