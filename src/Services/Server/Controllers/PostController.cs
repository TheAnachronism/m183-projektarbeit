﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using M183Project.Common.Post;
using M183Project.Common.Roles;
using M183Project.Server.Data;
using M183Project.Server.Helpers;
using M183Project.Shared;
using M183Project.Shared.Post;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace M183Project.Server.Controllers
{
    public class PostController : ControllerBase
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<PostController> _logger;

        public PostController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager, ILogger<PostController> logger)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetPost(Guid id)
        {
            var post = await _applicationDbContext.Posts.Include(x => x.OriginalPoster).SingleOrDefaultAsync(x => x.Id == id);
            if (post == null)
                return BadRequest();

            var user = await _userManager.GetUserAsync(User);

            var resultPost = new GetPostResponse(post.Id, post.Title, post.Content, post.Status,
                post.OriginalPoster.UserName, post.CreatedAt, post.LastEditedAt);

            if (post.Status == PostStatus.Published)
                return Ok(resultPost);

            if (user == null || post.OriginalPoster != user ||
                await _userManager.IsInRoleAsync(user, DatabaseRoles.Administrator) == false)
                return BadRequest();

            return Ok(resultPost);
        }

        [Route("api/posts")]
        public async Task<IActionResult> GetPosts()
        {
            var posts = await _applicationDbContext.Posts.Where(x => x.Status == PostStatus.Published).Include(x => x.OriginalPoster).ToListAsync();
            return Ok(posts.Select(x => new GetPostResponse(x.Id, x.Title, x.Content, x.Status, x.OriginalPoster.UserName,
                x.CreatedAt, x.LastEditedAt)));
        }

        [HttpGet]
        public async Task<IActionResult> GetPosts([FromQuery] GetVirtualizeRequest request)
        {
            var query = await QueryFilterHelper.GetPostFilterAsync(_userManager, User);

            var (startIndex, pageSize) = request;

            var count = await _applicationDbContext.Posts.Where(query).CountAsync();

            var posts = await _applicationDbContext.Posts
                .Where(query)
                .OrderByDescending(x => x.CreatedAt)
                .Skip(startIndex)
                .Take(pageSize)
                .Include(x => x.OriginalPoster)
                .ToListAsync();

            return Ok(new GetVirtualizeResponse<GetPostResponse>(posts.Select(x => new GetPostResponse(x.Id,
                    x.Title,
                    x.Content,
                    x.Status,
                    x.OriginalPoster.UserName,
                    x.CreatedAt,
                    x.LastEditedAt)),
                count));
        }

        [HttpGet]
        [Authorize(Roles = DatabaseRoles.Administrator)]
        public async Task<IActionResult> GetPostsWithStatus(PostStatus status)
        {
            var posts = await _applicationDbContext.Posts.Where(x => x.Status == status).ToListAsync();
            return Ok(posts.Select(x => new GetPostResponse(x.Id, x.Title, x.Content, x.Status, x.OriginalPoster.UserName, x.CreatedAt, x.LastEditedAt)));
        }

        [HttpGet]
        [Authorize(Roles = DatabaseRoles.Administrator)]
        public async Task<IActionResult> CreateDemoData()
        {
            var user = await _userManager.GetUserAsync(User);

            var posts = new List<Post>();
            for (var i = 0; i < 50; i++)
            {
                posts.Add(new Post
                {
                    Status = PostStatus.Hidden,
                    OriginalPoster = user,
                    Title = $"post {i}",
                    Content = $"Some demo post.",
                    CreatedAt = DateTime.UtcNow,
                    LastEditedAt = DateTime.UtcNow,
                    Comments = new List<Comment>
                    {
                        new() {CommentContent = $"Comment 1 for {i}", CreatedAt = DateTime.UtcNow, OriginalPoster = user},
                        new() {CommentContent = $"Comment 2 for {i}", CreatedAt = DateTime.UtcNow, OriginalPoster = user}
                    }
                });
            }
            
            await _applicationDbContext.Posts.AddRangeAsync(posts);
            await _applicationDbContext.SaveChangesAsync();
            
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetPostFromComment(Guid id)
        {
            var comment = await _applicationDbContext.Comments.Include(x => x.Post).SingleAsync(x => x.Id == id);
            var post = await _applicationDbContext.Posts.Include(x => x.OriginalPoster).SingleAsync(x => x == comment.Post);

            return Ok(new GetPostResponse(post.Id,
                post.Title,
                post.Content,
                post.Status,
                post.OriginalPoster.UserName,
                post.CreatedAt,
                post.LastEditedAt));
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreatePost([FromBody] CreatePostRequest request)
        {
            var poster = await _userManager.GetUserAsync(User);
            var post = new Post
            {
                Content = request.Content,
                Title = request.Title,
                Status = PostStatus.Hidden,
                OriginalPoster = poster,
                CreatedAt = DateTime.UtcNow,
                LastEditedAt = DateTime.UtcNow
            };

            var result = await _applicationDbContext.Posts.AddAsync(post);
            await _applicationDbContext.SaveChangesAsync();

            return Ok(result.Entity.Id);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> DeletePost([FromBody] DeletePostRequest request)
        {
            var post = await _applicationDbContext.Posts.FindAsync(request.Id);
            var user = await _userManager.GetUserAsync(User);
            if (post.OriginalPoster != user || await _userManager.IsInRoleAsync(user, DatabaseRoles.Administrator) == false)
            {
                _logger.LogWarning($"Failed to delete post, requested by {user.UserName}");
                return Unauthorized();
            }

            post.Status = PostStatus.Deleted;
            await _applicationDbContext.SaveChangesAsync();

            return Ok();
        }

        [HttpPost]
        [Authorize(Roles = DatabaseRoles.Administrator)]
        public async Task<IActionResult> HidePost([FromBody] HidePostRequest request)
        {
            var post = await _applicationDbContext.Posts.FindAsync(request.Id);
            post.Status = PostStatus.Hidden;
            await _applicationDbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpGet]
        [Authorize(Roles = DatabaseRoles.Administrator)]
        public async Task<IActionResult> CheckPublishPost(Guid id)
        {
            var user = await _userManager.GetUserAsync(User);
            var is2FaEnabled = await _userManager.GetTwoFactorEnabledAsync(user);
            if (!is2FaEnabled)
                return BadRequest();

            return Ok();
        }

        [HttpPost]
        [Authorize(Roles = DatabaseRoles.Administrator)]
        public async Task<IActionResult> PublishPost([FromBody] PublishPostRequest request)
        {
            var post = await _applicationDbContext.Posts.FindAsync(request.Id);
            var user = await _userManager.GetUserAsync(User);
            var verificationCode = request.Code.Replace(" ", string.Empty).Replace("-", string.Empty);

            var is2FaTokenValid = await _userManager.VerifyTwoFactorTokenAsync(user,
                _userManager.Options.Tokens.AuthenticatorTokenProvider, verificationCode);
            if (!is2FaTokenValid)
                return Unauthorized();

            post.Status = PostStatus.Published;
            await _applicationDbContext.SaveChangesAsync();

            return Ok();
        }
    }
}
