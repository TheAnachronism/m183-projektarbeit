﻿using System;
using System.Linq;
using System.Threading.Tasks;
using M183Project.Server.Data;
using M183Project.Server.Helpers;
using M183Project.Shared;
using M183Project.Shared.Comment;
using M183Project.Shared.Post;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using Microsoft.Extensions.Logging;

namespace M183Project.Server.Controllers
{
    public class CommentController : ControllerBase
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<CommentController> _logger;

        public CommentController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager, ILogger<CommentController> logger)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetCommentsFromPost(Guid id, [FromQuery] GetVirtualizeRequest request)
        {
            var query = await QueryFilterHelper.GetCommentFilterAsync(_userManager, User);

            var post = await _applicationDbContext.Posts.FindAsync(id);
            if (post == null)
                return BadRequest();

            var count = await _applicationDbContext.Comments.Where(x => x.Post == post).Where(query).CountAsync();

            var (startIndex, pageSize) = request;
            var comments = await _applicationDbContext.Comments
                .Where(x => x.Post == post)
                .Where(query)
                .Include(x => x.Post)
                .Include(x => x.OriginalPoster)
                .OrderByDescending(x => x.CreatedAt)
                .Skip(startIndex)
                .Take(pageSize)
                .ToListAsync();

            return Ok(new GetVirtualizeResponse<GetCommentResponse>(comments.Select(x =>
                    new GetCommentResponse(x.Post.Id,
                        x.Post.Title,
                        x.OriginalPoster.UserName,
                        x.CreatedAt,
                        x.CommentContent)),
                count));
        }

        [HttpGet]
        public async Task<IActionResult> GetCommentsFromUser(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                _logger.LogInformation($"Request comments for non-existent user: {username}.");
                return BadRequest();
            }

            var comments = await _applicationDbContext.Comments.Where(x => x.OriginalPoster == user).ToListAsync();
            return Ok(comments.Select(x =>
                new GetCommentResponse(x.Post.Id, x.Post.Title, user.UserName, x.CreatedAt, x.CommentContent)));
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostComment([FromBody] PostCommentRequest request)
        {
            var user = await _userManager.GetUserAsync(User);

            var (postId, content) = request;
            var post = await _applicationDbContext.Posts.FindAsync(postId);
            if (post == null)
            {
                _logger.LogWarning("Failed to post comment to non-existent post.");
                return BadRequest();
            }

            var comment = new Comment
            {
                CommentContent = content,
                OriginalPoster = user,
                CreatedAt = DateTime.UtcNow,
                Post = post
            };

            await _applicationDbContext.Comments.AddAsync(comment);
            await _applicationDbContext.SaveChangesAsync();

            return Ok();
        }
    }
}
