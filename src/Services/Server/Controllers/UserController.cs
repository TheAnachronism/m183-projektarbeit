﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using M183Project.Common.Post;
using M183Project.Common.Roles;
using M183Project.Server.Data;
using M183Project.Server.Helpers;
using M183Project.Shared;
using M183Project.Shared.Comment;
using M183Project.Shared.Post;
using M183Project.Shared.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace M183Project.Server.Controllers
{
    public class UserController : ControllerBase
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        [Authorize(Roles = DatabaseRoles.Administrator)]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _applicationDbContext.Users.ToListAsync();
            var userRoleMap = users.Select(async x => new KeyValuePair<ApplicationUser, string>(x,
                (await _userManager.GetRolesAsync(x)).Single())).ToList();

            await Task.WhenAll(userRoleMap);

            return Ok(userRoleMap.Select(x => new GetUserManageResponse(x.Result.Key.UserName, x.Result.Value)));
        }

        [HttpGet]
        public async Task<IActionResult> GetUser(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
                return BadRequest();
            
            var comments = await _applicationDbContext.Comments.Where(x => x.OriginalPoster == user).Include(x => x.Post).ToListAsync();

            return Ok(new GetUserResponse(user.UserName));
        }

        [HttpGet]
        public async Task<ActionResult> GetPostsFromUser(string userName, [FromQuery] GetVirtualizeRequest request)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
                return BadRequest();

            var query = await QueryFilterHelper.GetPostFilterAsync(_userManager, User);

            var count = await _applicationDbContext.Posts.Where(x => x.OriginalPoster == user).Where(query)
                .CountAsync();

            var (startIndex, pageSize) = request;
            var posts = await _applicationDbContext.Posts
                .Where(x => x.OriginalPoster == user)
                .Where(query)
                .OrderByDescending(x => x.CreatedAt)
                .Skip(startIndex)
                .Take(pageSize)
                .ToListAsync();

            return Ok(new GetVirtualizeResponse<GetPostResponse>(posts.Select(x => new GetPostResponse(x.Id,
                x.Title,
                x.Content,
                x.Status,
                user.UserName,
                x.CreatedAt,
                x.LastEditedAt)), count));
        }

        [HttpGet]
        public async Task<IActionResult> GetCommentsFromUser(string userName, [FromQuery] GetVirtualizeRequest request)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
                return BadRequest();

            var commentQuery = await QueryFilterHelper.GetCommentFilterAsync(_userManager, User);

            var count = await _applicationDbContext.Comments.Where(x => x.OriginalPoster == user).Where(commentQuery)
                .CountAsync();

            var comments = await _applicationDbContext.Comments
                .Where(x => x.OriginalPoster == user)
                .Where(commentQuery)
                .Include(x => x.Post)
                .OrderByDescending(x => x.CreatedAt)
                .Skip(request.StartIndex)
                .Take(request.PageSize)
                .ToListAsync();

            return Ok(new GetVirtualizeResponse<GetCommentResponse>(comments.Select(x =>
                new GetCommentResponse(x.Post.Id,
                    x.Post.Title,
                    x.OriginalPoster.UserName,
                    x.CreatedAt, 
                    x.CommentContent)), count));
        }

        [HttpPost]
        [Authorize(Roles = DatabaseRoles.Administrator)]
        public async Task<IActionResult> SetUserRole([FromBody] SetUserRoleRequest request)
        {
            var (userName, newRole) = request;

            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
                return BadRequest();
            
            var role = await _roleManager.FindByNameAsync(newRole);
            await _userManager.RemoveFromRolesAsync(user, await _userManager.GetRolesAsync(user));
            await _userManager.AddToRoleAsync(user, role.Name);
            
            return Ok();
        }
    }
}
