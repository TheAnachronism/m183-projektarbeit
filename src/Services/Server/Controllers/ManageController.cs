﻿using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using M183Project.Server.Data;
using M183Project.Server.Services;
using M183Project.Shared.Manage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace M183Project.Server.Controllers
{
    [Authorize]
    public class ManageController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly SmsTokenService _smsTokenService;
        private readonly UrlEncoder _urlEncoder;
        private readonly ILogger<ManageController> _logger;

        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";

        public ManageController(UserManager<ApplicationUser> userManager, SmsTokenService smsTokenService,
            SignInManager<ApplicationUser> signInManager, UrlEncoder urlEncoder, ILogger<ManageController> logger)
        {
            _userManager = userManager;
            _smsTokenService = smsTokenService;
            _signInManager = signInManager;
            _urlEncoder = urlEncoder;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetPhoneNumberInfo()
        {
            var user = await _userManager.GetUserAsync(User);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            return Ok(new GetPhoneNumberInfoResult(phoneNumber));
        }

        [HttpPost]
        public async Task<IActionResult> ChangePhoneNumber([FromBody] ChangePhoneNumberRequest request)
        {
            var user = await _userManager.GetUserAsync(User);
            var changeToken = await _userManager.GenerateChangePhoneNumberTokenAsync(user, request.PhoneNumber);
            var smsChangeToken = await _smsTokenService.CreateSmsToken(user, changeToken);
            _logger.LogInformation($"Changed phone number of user {user.UserName}");
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> VerifyChangedPhoneNumber([FromBody] VerifyChangePhoneNumberRequest request)
        {
            var user = await _userManager.GetUserAsync(User);
            var (isSuccessful, smsToken) = await _smsTokenService.VerifySmsToken(user, request.Token);
            if (!isSuccessful)
            {
                _logger.LogWarning("Invalid phone number verification code sent.");
                return BadRequest();
            }

            await _userManager.ChangePhoneNumberAsync(user, request.PhoneNumber, smsToken.Note);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordRequest request)
        {
            var user = await _userManager.GetUserAsync(User);
            var changePasswordResult =
                await _userManager.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);

            if (!changePasswordResult.Succeeded)
                return Conflict(changePasswordResult.Errors.ToDictionary(x => x.Code, x => x.Description));

            await _signInManager.RefreshSignInAsync(user);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> TwoFactor()
        {
            var user = await _userManager.GetUserAsync(User);
            var hasAuthenticator = await _userManager.GetAuthenticatorKeyAsync(user) != null;
            var twoFactorEnabled = await _userManager.GetTwoFactorEnabledAsync(user);
            var twoFactorClientRemembered = await _signInManager.IsTwoFactorClientRememberedAsync(user);
            var recoveryCodesLeft = await _userManager.CountRecoveryCodesAsync(user);

            return Ok(new TwoFactorResponse(hasAuthenticator, twoFactorEnabled, twoFactorClientRemembered,
                recoveryCodesLeft));
        }

        [HttpPost]
        public async Task<IActionResult> TwoFactorForget()
        {
            await _signInManager.ForgetTwoFactorClientAsync();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> CheckTwoFactorGenerate()
        {
            var user = await _userManager.GetUserAsync(User);
            var isTwoFactorEnabled = await _userManager.GetTwoFactorEnabledAsync(user);
            if (!isTwoFactorEnabled)
                return BadRequest();

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> TwoFactorGenerate()
        {
            var user = await _userManager.GetUserAsync(User);
            var isTwoFactorEnabled = await _userManager.GetTwoFactorEnabledAsync(user);
            if (!isTwoFactorEnabled)
                return BadRequest();

            var recoveryCodes = await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 10);
            return Ok(new TwoFactorGenerateResponse(recoveryCodes.ToArray()));
        }

        [HttpGet]
        public async Task<IActionResult> CheckTwoFactorEnable()
        {
            var user = await _userManager.GetUserAsync(User);
            var result = await LoadSharedKeyAndQrCodeUriAsync(user);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> TwoFactorEnable([FromBody] TwoFactorEnableRequest request)
        {
            var user = await _userManager.GetUserAsync(User);
            var verificationCode = request.Code.Replace(" ", string.Empty).Replace("-", string.Empty);

            var is2FaTokenValid = await _userManager.VerifyTwoFactorTokenAsync(user,
                _userManager.Options.Tokens.AuthenticatorTokenProvider, verificationCode);
            if (!is2FaTokenValid)
                return BadRequest();

            await _userManager.SetTwoFactorEnabledAsync(user, true);
            if (await _userManager.CountRecoveryCodesAsync(user) != 0)
                return Ok(new TwoFactorEnableResponse(null, false));

            var recoveryCodes = await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 10);
            return Ok(new TwoFactorEnableResponse(recoveryCodes.ToArray(), true));
        }

        [HttpPost]
        public async Task<IActionResult> DisableAuthenticator()
        {
            var user = await _userManager.GetUserAsync(User);

            var disable2FaResult = await _userManager.SetTwoFactorEnabledAsync(user, false);
            if (!disable2FaResult.Succeeded)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ResetAuthenticator()
        {
            var user = await _userManager.GetUserAsync(User);
            await _userManager.SetTwoFactorEnabledAsync(user, false);
            await _userManager.ResetAuthenticatorKeyAsync(user);

            await _signInManager.RefreshSignInAsync(user);
            return Ok();
        }

        private async Task<TwoFactorInfoResponse> LoadSharedKeyAndQrCodeUriAsync(ApplicationUser user)
        {
            var unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            if (string.IsNullOrEmpty(unformattedKey))
            {
                await _userManager.ResetAuthenticatorKeyAsync(user);
                unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            }

            var sharedKey = FormatKey(unformattedKey);
            var email = await _userManager.GetEmailAsync(user);
            return new TwoFactorInfoResponse(sharedKey, GenerateQrCodeUri(email, unformattedKey));
        }

        private static string FormatKey(string unformattedKey)
        {
            var builder = new StringBuilder();
            var currentPosition = 0;
            while (currentPosition + 4 < unformattedKey.Length)
            {
                builder.Append(unformattedKey.Substring(currentPosition, 4)).Append(" ");
                currentPosition += 4;
            }

            if (currentPosition < unformattedKey.Length)
                builder.Append(unformattedKey.Substring(currentPosition));

            return builder.ToString().ToLowerInvariant();
        }

        private string GenerateQrCodeUri(string email, string unformattedKey) => string.Format(AuthenticatorUriFormat,
            _urlEncoder.Encode("M183 Project"), _urlEncoder.Encode(email), unformattedKey);
    }
}
