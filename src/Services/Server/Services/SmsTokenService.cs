﻿using System;
using System.Linq;
using System.Threading.Tasks;
using M183Project.Server.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace M183Project.Server.Services
{
    public record SmsTokenValidationResult(bool IsSuccessful, SmsToken Token);

    public class SmsTokenService
    {
        private readonly ApplicationDbContext _context;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly SmsSenderService _smsSenderService;

        private readonly Random _random = new();

        public SmsTokenService(ApplicationDbContext context, IPasswordHasher<ApplicationUser> passwordHasher, SmsSenderService smsSenderService)
        {
            _context = context;
            _passwordHasher = passwordHasher;
            _smsSenderService = smsSenderService;
        }

        public async Task<SmsToken> CreateSmsToken(ApplicationUser user, string note = null)
        {
            var generatedToken = _random.Next(100000, 1000000).ToString("D6");
            var token = _passwordHasher.HashPassword(user, generatedToken);
            var smsToken = new SmsToken { ApplicationUser = user, Token = token, Note = note };
            var result = await _context.SmsTokens.AddAsync(smsToken);
            await _context.SaveChangesAsync();

            await _smsSenderService.SendSms(user.PhoneNumber, generatedToken);

            return result.Entity;
        }

        public async Task<SmsTokenValidationResult> VerifySmsToken(ApplicationUser user, string token)
        {
            await CleanupPastTokens(user);
            var tokens = await _context.SmsTokens.Where(x => x.ApplicationUser == user).ToListAsync();
            var foundToken = tokens.SingleOrDefault(x => _passwordHasher.VerifyHashedPassword(user,
                                                     x.Token,
                                                     token) ==
                                                 PasswordVerificationResult.Success);

            return new SmsTokenValidationResult(foundToken != null, foundToken);
        }

        public async Task RemoveToken(SmsToken token)
        {
            var foundToken = await _context.SmsTokens.SingleAsync(x => x.Id == token.Id);
            _context.SmsTokens.RemoveRange(foundToken);
            await _context.SaveChangesAsync();
        }

        private async Task CleanupPastTokens(ApplicationUser user)
        {
            var tokens = await _context.SmsTokens.Where(x =>
                x.ApplicationUser == user && x.CreatedAt < DateTime.Now.AddMinutes(-5)).ToListAsync();
            _context.SmsTokens.RemoveRange(tokens);

            await _context.SaveChangesAsync();
        }
    }
}
