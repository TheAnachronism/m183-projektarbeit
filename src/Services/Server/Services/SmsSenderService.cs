﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace M183Project.Server.Services
{
    public class SmsSenderService
    {
        private const string SmsGateway = "https://m183.gibz-informatik.ch/api/sms/message";
        
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        private readonly ILogger<SmsSenderService> _logger;

        public SmsSenderService(IHttpClientFactory httpClientFactory, IConfiguration configuration, ILogger<SmsSenderService> logger)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _configuration = configuration.GetSection("SmsGateway");
        }

        public async Task SendSms(string phoneNumber, string message)
        {
            phoneNumber = phoneNumber.Replace("+", "");
            
            var httpClient = _httpClientFactory.CreateClient();
            var content = new {MobileNumber = phoneNumber, Message = message};

            var request = new HttpRequestMessage(HttpMethod.Post, SmsGateway);
            request.Headers.Add("x-api-key", _configuration.GetValue<string>("ApiKey"));
            request.Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json");

            _logger.LogInformation("Sending sms to gateway");
            var result = await httpClient.SendAsync(request);
            result.EnsureSuccessStatusCode();
        }
    }
}
