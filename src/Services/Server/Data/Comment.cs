﻿using System;
using System.ComponentModel.DataAnnotations;

namespace M183Project.Server.Data
{
    public class Comment
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Post Post { get; set; }
        public ApplicationUser OriginalPoster { get; set; }

        [StringLength(200)]
        public string CommentContent { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
