﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace M183Project.Server.Data
{
    public class ApplicationUser : IdentityUser
    {
        public List<SmsToken> SmsTokens { get; set; }
        public List<Post> Posts { get; set; }
        public List<Comment> Comments { get; set; }
    }
}
