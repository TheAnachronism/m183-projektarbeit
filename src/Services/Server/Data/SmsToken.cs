﻿using System;

namespace M183Project.Server.Data
{
    public class SmsToken
    {
        public Guid Id { get; set; }
        public string Token { get; set; }
        public string Note { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public ApplicationUser ApplicationUser { get; set; }
    }
}
