﻿using System;
using System.Collections.Generic;
using M183Project.Common.Post;

namespace M183Project.Server.Data
{
    public class Post
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public List<Comment> Comments { get; set; }
        public ApplicationUser OriginalPoster { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }
        public PostStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastEditedAt { get; set; }
    }
}
