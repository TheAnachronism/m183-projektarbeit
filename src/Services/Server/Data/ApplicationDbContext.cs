﻿using System;
using System.Collections.Generic;
using M183Project.Common.Roles;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace M183Project.Server.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Post>().Property(x => x.Status).HasConversion<string>();

            builder.Entity<IdentityRole>().HasData(new List<IdentityRole>
            {
                {
                    new()
                    {
                        Id = "05e0af3a-e67e-4c10-97bf-d42930484e1a",
                        ConcurrencyStamp = "05e0af3a-e67e-4c10-97bf-d42930484e1a",
                        Name = DatabaseRoles.Administrator,
                        NormalizedName = DatabaseRoles.Administrator.ToUpper()
                    }
                },
                {
                    new()
                    {
                        Id = "d7be1a63-bd24-4bbc-9872-9be3d1f82568",
                        ConcurrencyStamp = "d7be1a63-bd24-4bbc-9872-9be3d1f82568",
                        Name = DatabaseRoles.User,
                        NormalizedName = DatabaseRoles.User.ToUpper()
                    }
                }
            });

            base.OnModelCreating(builder);
        }

        public DbSet<SmsToken> SmsTokens { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}
