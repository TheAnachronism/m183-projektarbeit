﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using M183Project.Client;
using M183Project.Common;
using M183Project.Server.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace M183Project.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        public static async Task MainAsync(string[] args)
        {
            try
            {
                SetupSerilog();

                Log.Information("Starting up...");
                var apiHost = CreateHostBuilder(args).Build();

                var config = apiHost.Services.GetRequiredService<IConfiguration>();
                if (config.GetValue<string>("ASPNETCORE_APPLY_MIGRATIONS_ON_STARTUP") == "true")
                    await apiHost.MigrateDatabase<ApplicationDbContext>(apiHost.Services.GetService<ILogger<Program>>());

                if (config.GetValue<string>("ASPNETCORE_ENSURE_ADMINISTRATOR_USER_IS_CREATED") == "true")
                    await apiHost.EnsureAdminUser<ApplicationUser>(apiHost.Services
                        .GetService<ILogger<Program>>());

                await apiHost.RunAsync();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Application startup failed...");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static void SetupSerilog()
        {
            var configuration = new ConfigurationBuilder().AddEnvironmentVariables().Build();
            var loggerConfig = new LoggerConfiguration().ConfigureDefaultLogging(configuration, Assembly.GetExecutingAssembly());

            Log.Logger = loggerConfig.CreateLogger();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog(Log.Logger)
                .ConfigureLogging((context, builder) =>
                {
                    builder.ClearProviders();
                    builder.AddSerilog(Log.Logger);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
