﻿using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Manage
{
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [Required]
        [Compare(nameof(NewPassword), ErrorMessage = "Passwords do not match!")]
        [DataType(DataType.Password)]
        public string NewPasswordConfirm { get; set; }
    }
}
