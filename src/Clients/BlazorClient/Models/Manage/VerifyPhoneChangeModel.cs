﻿using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Manage
{
    public class VerifyPhoneChangeModel
    {
        [Required]
        public string Token { get; set; }   
    }
}
