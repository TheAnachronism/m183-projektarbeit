﻿namespace M183Project.Client.Models.Manage.TwoFactor
{
    public class TwoFactorEnableAuthenticatorModel
    {
        public string SharedKey { get; set; }
        public string AuthenticatorUri { get; set; }
    }
}
