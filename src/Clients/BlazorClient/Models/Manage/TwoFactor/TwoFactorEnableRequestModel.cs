﻿using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Manage.TwoFactor
{
    public class TwoFactorEnableRequestModel
    {
        [Required]
        public string Code { get; set; }
    }
}
