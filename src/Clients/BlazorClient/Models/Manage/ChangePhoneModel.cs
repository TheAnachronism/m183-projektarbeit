﻿using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Manage
{
    public class ChangePhoneModel
    {
        [Phone]
        public string CurrentPhoneNumber { get; set; }
        [Phone]
        [Required]
        public string NewPhoneNumber { get; set; }
    }
}
