﻿using System;
using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Comments
{
    public class CommentModel
    {
        public string OriginalPosterUsername { get; set; }
        public DateTime CreatedAt { get; set; }
        [StringLength(200)]
        public string Content { get; set; }
        public Guid PostId { get; set; }
        public string PostTitle { get; set; }
    }
}
