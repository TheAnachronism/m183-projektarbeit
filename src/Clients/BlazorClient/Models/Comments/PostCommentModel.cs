﻿using System;
using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Comments
{
    public class PostCommentModel
    {
        [Required]
        [StringLength(200, ErrorMessage = "A comment cannot be longer then 200 characters!")]
        public string CommentContent { get; set; }
        public Guid PostId { get; set; }
    }
}
