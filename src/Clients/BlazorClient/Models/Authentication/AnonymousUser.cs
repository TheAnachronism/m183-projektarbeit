﻿namespace M183Project.Client.Models.Authentication
{
    public class AnonymousUser : CurrentUserModel
    {
        public AnonymousUser()
        {
            IsAuthenticated = false;
        }
    }
}
