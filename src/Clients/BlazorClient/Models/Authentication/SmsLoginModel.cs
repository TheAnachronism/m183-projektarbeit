﻿using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Authentication
{
    public class SmsLoginModel
    {
        [Required]
        public int SmsToken { get; set; }
    }
}
