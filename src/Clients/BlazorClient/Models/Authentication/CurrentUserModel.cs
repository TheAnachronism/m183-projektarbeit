﻿using System.Collections.Generic;

namespace M183Project.Client.Models.Authentication
{
    public class CurrentUserModel
    {
        public bool IsAuthenticated { get; set; }
        public string UserName { get; set; }
        public IEnumerable<KeyValuePair<string, string>> Claims { get; set; }
    }
}
