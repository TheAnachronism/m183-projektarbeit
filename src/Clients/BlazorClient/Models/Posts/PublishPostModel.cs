﻿using System;
using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Posts
{
    public class PublishPostModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string TwoFactorCode { get; set; }
    }
}
