﻿using System;
using M183Project.Common.Post;

namespace M183Project.Client.Models.Posts
{
    public class PostModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string OriginalPoster { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastEditedAt { get; set; }
        public PostStatus Status { get; set; }
    }
}
