﻿using System.Collections.Generic;

namespace M183Project.Client.Models.Posts
{
    public class VirtualizeModel<T>
    {
        public IEnumerable<T> Items { get; set; }
        public int TotalSize { get; set; }
    }
}
