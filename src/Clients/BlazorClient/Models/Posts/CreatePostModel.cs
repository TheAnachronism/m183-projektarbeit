﻿using System.ComponentModel.DataAnnotations;

namespace M183Project.Client.Models.Posts
{
    public class CreatePostModel
    {
        [Required]
        public string Title { get; set; }

        [Required] public string Content { get; set; } = string.Empty;
    }
}
