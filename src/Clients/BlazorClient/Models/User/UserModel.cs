﻿using System.Collections.Generic;
using M183Project.Client.Models.Comments;

namespace M183Project.Client.Models.User
{
    public class UserModel
    {
        public string UserName { get; set; }
        public IEnumerable<CommentModel> Comments { get; set; }    
    }
}
