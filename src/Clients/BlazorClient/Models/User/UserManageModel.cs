﻿namespace M183Project.Client.Models.User
{
    public class UserManageModel
    {
        public string UserName { get; set; }
        public string Role { get; set; }
    }
}
