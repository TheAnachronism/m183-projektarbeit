﻿using QRCoder;

namespace M183Project.Client.Helpers
{
    public static class QrCodeHelper
    {
        public static byte[] GetQrCode(string[] data)
        {
            var generator = new QRCoder.QRCodeGenerator();
            var qrData = generator.CreateQrCode(string.Join("", data), QRCodeGenerator.ECCLevel.Q);
            var qrCode = new QRCoder.PngByteQRCode(qrData);
            return qrCode.GetGraphic(20);
        }
    }
}
