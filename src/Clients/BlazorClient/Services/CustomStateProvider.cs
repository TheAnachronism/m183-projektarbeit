﻿using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Threading.Tasks;
using M183Project.Client.Models.Authentication;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Logging;

namespace M183Project.Client.Services
{
    public class CustomStateProvider : AuthenticationStateProvider
    {
        private readonly IAuthService _authService;
        private readonly ILogger<CustomStateProvider> _logger;

        private CurrentUserModel _currentUser;

        public CustomStateProvider(IAuthService authService, ILogger<CustomStateProvider> logger)
        {
            _authService = authService;
            _logger = logger;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var identity = new ClaimsIdentity();
            try
            {
                var userInfo = await GetCurrentUser();
                if (userInfo.IsAuthenticated)
                {
                    var claims = new[] {new Claim(ClaimTypes.Name, _currentUser.UserName)}.Concat(
                        _currentUser.Claims.Select(c => new Claim(c.Key, c.Value))).ToList();
                    identity = new ClaimsIdentity(claims, "Server Authentication");
                }
            }
            catch (HttpRequestException e)
            {
                _logger.LogError(e, $"Request failed.");
            }

            return new AuthenticationState(new ClaimsPrincipal(identity));
        }

        private async Task<CurrentUserModel> GetCurrentUser(bool force = false)
        {
            if (_currentUser != null && _currentUser.IsAuthenticated && !force) return _currentUser;
            _currentUser = await _authService.CurrentUserInfo();
            if (_currentUser is AnonymousUser)
                await Logout();

            return _currentUser;
        }

        public async Task RefreshAuthenticationState()
        {
            await GetCurrentUser(true);
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }

        public async Task Logout(bool userWasDeleted = false)
        {
            if (!userWasDeleted)
                await _authService.Logout();

            _currentUser = null;
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }

        public async Task Login(LoginModel model)
        {
            await _authService.Login(model);
        }

        public async Task SmsLogin(SmsLoginModel model, LoginModel loginModel)
        {
            await _authService.SmsLogin(model, loginModel);
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }
        
        public async Task Register(RegisterModel model)
        {
            await _authService.Register(model);
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }
    }
}
