﻿using System.Threading.Tasks;
using M183Project.Client.Models.Authentication;
using M183Project.Shared.Authentication;

namespace M183Project.Client.Services
{
    public interface IAuthService
    {
        Task Login(LoginModel model);
        Task SmsLogin(SmsLoginModel model, LoginModel loginModel);
        Task Register(RegisterModel model);
        Task Logout();
        Task<CurrentUserModel> CurrentUserInfo();
    }
}
