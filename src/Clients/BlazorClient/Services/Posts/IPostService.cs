﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using M183Project.Client.Models.Comments;
using M183Project.Client.Models.Posts;
using M183Project.Common.Post;

namespace M183Project.Client.Services.Posts
{
    public interface IPostService
    {
        Task<PostModel> GetPost(Guid id);
        Task<VirtualizeModel<PostModel>> GetPosts(int startIndex, int page);
        Task<VirtualizeModel<PostModel>> GetPostsFromUser(string username, int startIndex, int page);
        Task<IEnumerable<PostModel>> GetPosts(PostStatus status);
        Task<PostModel> GetPostFromCommentId(Guid commentId);
        Task<Guid> CreatePost(CreatePostModel model);
        Task DeletePost(Guid id);
        Task HidePost(Guid id);
        Task PublishPost(PublishPostModel model);
        Task CheckPublishPost(Guid id);
        Task<VirtualizeModel<CommentModel>> GetComments(Guid postId, int startIndex, int page);
        Task<VirtualizeModel<CommentModel>> GetCommentsForUser(string username, int startIndex, int page);
        Task PostComment(PostCommentModel model);
    }
}
