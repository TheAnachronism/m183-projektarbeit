﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Blazorise;
using M183Project.Client.Models.Comments;
using M183Project.Client.Models.Posts;
using M183Project.Common.Post;
using M183Project.Shared;
using M183Project.Shared.Comment;
using M183Project.Shared.Post;
using M183Project.Shared.User;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;

namespace M183Project.Client.Services.Posts
{
    public class PostService : IPostService
    {
        private readonly HttpClient _httpClient;

        public PostService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<PostModel> GetPost(Guid id)
        {
            var result = await _httpClient.GetAsync($"api/Post/GetPost?id={id}");

            var content = JsonConvert.DeserializeObject<GetPostResponse>(await result.Content.ReadAsStringAsync());
            return new PostModel
            {
                Title = content.Title,
                Content = content.Content,
                Status = content.Status,
                CreatedAt = content.UtcCreatedAt,
                LastEditedAt = content.UtcLastEdited,
                OriginalPoster = content.OriginalPosterUsername,
                Id = content.Id
            };
        }

        public async Task<VirtualizeModel<PostModel>> GetPosts(int startIndex, int page)
        {
            var queryString = new Dictionary<string, string>
            {
                [nameof(GetVirtualizeRequest.StartIndex)] = startIndex.ToString(),
                [nameof(GetVirtualizeRequest.PageSize)] = page.ToString(),
            };

            var result = await _httpClient.GetAsync(QueryHelpers.AddQueryString("api/Post/GetPosts", queryString));
            result.EnsureSuccessStatusCode();
            var content = await result.Content.ReadAsStringAsync();
            var (getPostResponses, totalSize) = JsonConvert.DeserializeObject<GetVirtualizeResponse<GetPostResponse>>(content);

            return new VirtualizeModel<PostModel>
            {
                Items = getPostResponses.Select(x => new PostModel
                {
                    Content = x.Content,
                    Id = x.Id,
                    Status = x.Status,
                    Title = x.Title,
                    OriginalPoster = x.OriginalPosterUsername,
                    CreatedAt = x.UtcCreatedAt,
                    LastEditedAt = x.UtcLastEdited
                }),
                TotalSize = totalSize
            };
        }

        public async Task<VirtualizeModel<PostModel>> GetPostsFromUser(string username, int startIndex, int page)
        {
            var queryString = new Dictionary<string, string>
            {
                ["username"] = username,
                [nameof(GetVirtualizeRequest.StartIndex)] = startIndex.ToString(),
                [nameof(GetVirtualizeRequest.PageSize)] = page.ToString()
            };

            var result = await _httpClient.GetAsync(QueryHelpers.AddQueryString("api/User/GetPostsFromUser", queryString));
            result.EnsureSuccessStatusCode();

            var (getPostResponses, totalSize) = JsonConvert.DeserializeObject<GetVirtualizeResponse<GetPostResponse>>(await result.Content.ReadAsStringAsync());
            return new VirtualizeModel<PostModel>
            {
                Items = getPostResponses.Select(x => new PostModel
                {
                    Content = x.Content,
                    Status = x.Status,
                    OriginalPoster = x.OriginalPosterUsername,
                    CreatedAt = x.UtcCreatedAt,
                    Id = x.Id,
                    LastEditedAt = x.UtcLastEdited,
                    Title = x.Title
                }),
                TotalSize = totalSize
            };
        }

        public async Task<IEnumerable<PostModel>> GetPosts(PostStatus status)
        {
            var result = await _httpClient.GetAsync($"api/Posts?status={status.ToString()}");
            var content =
                JsonConvert.DeserializeObject<List<GetPostResponse>>(await result.Content.ReadAsStringAsync());

            return content.Select(x => new PostModel
            { Content = x.Content, Id = x.Id, Status = x.Status, Title = x.Title });
        }

        public async Task<PostModel> GetPostFromCommentId(Guid commentId)
        {
            var result = await _httpClient.GetAsync($"api/Post/GetPostFromComment?id={commentId}");
            result.EnsureSuccessStatusCode();

            var content = JsonConvert.DeserializeObject<GetPostResponse>(await result.Content.ReadAsStringAsync());
            return new PostModel
            {
                OriginalPoster = content.OriginalPosterUsername,
                CreatedAt = content.UtcCreatedAt,
                Content = content.Content,
                Id = content.Id,
                Status = content.Status,
                LastEditedAt = content.UtcLastEdited,
                Title = content.Title
            };
        }

        public async Task<Guid> CreatePost(CreatePostModel model)
        {
            var content =
                new StringContent(JsonConvert.SerializeObject(new CreatePostRequest(model.Title, model.Content)), Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync("api/Post/CreatePost", content);
            result.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Guid>(await result.Content.ReadAsStringAsync());
        }

        public async Task DeletePost(Guid id)
        {
            var content = new StringContent(JsonConvert.SerializeObject(new DeletePostRequest(id)), Encoding.UTF8,
                "application/json");
            var result = await _httpClient.PostAsync("api/Post/DeletePost", content);
            result.EnsureSuccessStatusCode();
        }

        public async Task HidePost(Guid id)
        {
            var content = new StringContent(JsonConvert.SerializeObject(new HidePostRequest(id)), Encoding.UTF8,
                "application/json");
            var result = await _httpClient.PostAsync("api/Post/HidePost", content);
            result.EnsureSuccessStatusCode();
        }

        public async Task PublishPost(PublishPostModel model)
        {
            var content =
                new StringContent(JsonConvert.SerializeObject(new PublishPostRequest(model.Id, model.TwoFactorCode)),
                    Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync("api/Post/PublishPost", content);
            result.EnsureSuccessStatusCode();
        }

        public async Task CheckPublishPost(Guid id)
        {
            var result = await _httpClient.GetAsync($"api/Posts/id={id}");
            result.EnsureSuccessStatusCode();
        }

        public async Task<VirtualizeModel<CommentModel>> GetComments(Guid postId, int startIndex, int page)
        {
            var queryString = new Dictionary<string, string>
            {
                ["id"] = postId.ToString(),
                [nameof(GetVirtualizeRequest.StartIndex)] = startIndex.ToString(),
                [nameof(GetVirtualizeRequest.PageSize)] = page.ToString()
            };

            var result =
                await _httpClient.GetAsync(QueryHelpers.AddQueryString("api/Comment/GetCommentsFromPost", queryString));
            result.EnsureSuccessStatusCode();

            var (getCommentResponses, totalSize) = JsonConvert.DeserializeObject<GetVirtualizeResponse<GetCommentResponse>>(
                await result.Content.ReadAsStringAsync());

            return new VirtualizeModel<CommentModel>
            {
                Items = getCommentResponses.Select(x => new CommentModel
                {
                    CreatedAt = x.CreatedAt,
                    Content = x.Content,
                    PostId = x.PostId,
                    OriginalPosterUsername = x.OriginalPosterUsername,
                    PostTitle = x.PostTitle
                }),
                TotalSize = totalSize
            };
        }

        public async Task<VirtualizeModel<CommentModel>> GetCommentsForUser(string username, int startIndex, int page)
        {
            var queryString = new Dictionary<string, string>
            {
                ["username"] = username,
                [nameof(GetVirtualizeRequest.StartIndex)] = startIndex.ToString(),
                [nameof(GetVirtualizeRequest.PageSize)] = page.ToString()
            };

            var result =
                await _httpClient.GetAsync(QueryHelpers.AddQueryString("api/User/GetCommentsFromUser", queryString));
            result.EnsureSuccessStatusCode();

            var (getCommentResponses, totalSize) = JsonConvert.DeserializeObject<GetVirtualizeResponse<GetCommentResponse>>(
                await result.Content.ReadAsStringAsync());

            return new VirtualizeModel<CommentModel>
            {
                Items = getCommentResponses.Select(x => new CommentModel
                {
                    Content = x.Content,
                    CreatedAt = x.CreatedAt,
                    PostId = x.PostId,
                    OriginalPosterUsername = x.OriginalPosterUsername,
                    PostTitle = x.PostTitle
                }),
                TotalSize = totalSize
            };
        }

        public async Task PostComment(PostCommentModel model)
        {
            var content = JsonConvert.SerializeObject(new PostCommentRequest(model.PostId, model.CommentContent));
            var result = await _httpClient.PostAsync("api/Comment/PostComment",
                new StringContent(content, Encoding.UTF8, "application/json"));
            result.EnsureSuccessStatusCode();
        }
    }
}