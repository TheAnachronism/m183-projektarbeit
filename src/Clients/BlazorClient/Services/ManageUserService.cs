﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using M183Project.Client.Models.Manage;
using M183Project.Shared.Manage;
using Newtonsoft.Json;

namespace M183Project.Client.Services
{
    public class ManageUserService
    {
        private readonly HttpClient _httpClient;

        public ManageUserService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task ChangePasswordAsync(ChangePasswordModel model)
        {
            var content = new StringContent(JsonConvert.SerializeObject(new ChangePasswordRequest(model.CurrentPassword, model.NewPassword, model.NewPasswordConfirm)), Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync("api/Manage/ChangePassword", content);
            if (result.StatusCode == HttpStatusCode.BadRequest)
                throw new AccessViolationException();

            result.EnsureSuccessStatusCode();
        }

        public async Task<ChangePhoneModel> GetPhoneNumberInfoAsync()
        {
            var result = await _httpClient.GetAsync("api/Manage/GetPhoneNumberInfo");
            result.EnsureSuccessStatusCode();

            var content = JsonConvert.DeserializeObject<GetPhoneNumberInfoResult>(await result.Content.ReadAsStringAsync());
            return new ChangePhoneModel { CurrentPhoneNumber = content.PhoneNumber };
        }

        public async Task ChangePhoneNumberAsync(ChangePhoneModel model)
        {
            var content = new StringContent(JsonConvert.SerializeObject(new ChangePhoneNumberRequest(model.NewPhoneNumber)), Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync("api/Manage/ChangePhoneNumber", content);
           
            result.EnsureSuccessStatusCode();
        }

        public async Task VerifyChangePhoneNumberAsync(VerifyPhoneChangeModel model, ChangePhoneModel changePhoneModel)
        {
            var content = new StringContent(JsonConvert.SerializeObject(new VerifyChangePhoneNumberRequest(changePhoneModel.NewPhoneNumber, model.Token)), Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync("api/Manage/VerifyChangedPhoneNumber", content);
            if (result.StatusCode == HttpStatusCode.BadRequest)
                throw new AccessViolationException();

            result.EnsureSuccessStatusCode();
        }
    }
}
