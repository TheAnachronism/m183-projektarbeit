﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using M183Project.Client.Models.Authentication;
using M183Project.Shared.Authentication;
using Newtonsoft.Json;

namespace M183Project.Client.Services
{
    public class AuthenticationService : IAuthService
    {
        private readonly HttpClient _httpClient;

        public AuthenticationService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task Login(LoginModel model)
        {
            var result = await _httpClient.PostAsync("api/Authentication/Login",
                new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            switch (result.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                    throw new UnauthorizedAccessException("Invalid credentials");
                default:
                    result.EnsureSuccessStatusCode();
                    break;
            }
        }

        public async Task SmsLogin(SmsLoginModel model, LoginModel loginModel)
        {
            var result = await _httpClient.PostAsync("api/Authentication/LoginSms",
                new StringContent(JsonConvert.SerializeObject(new SmsLoginRequest(loginModel.UserName,
                        loginModel.Password,
                        model.SmsToken.ToString())),
                    Encoding.UTF8,
                    "application/json"));
            switch (result.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                    throw new UnauthorizedAccessException("Invalid Credentials!");
                default:
                    result.EnsureSuccessStatusCode();
                    break;
            }
        }

        public async Task Register(RegisterModel model)
        {
            var result = await _httpClient.PostAsync("api/Authentication/Register",
                new StringContent(JsonConvert.SerializeObject(new RegisterRequest
                {
                    Password = model.Password,
                    ConfirmPassword = model.PasswordConfirm,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    UserName = model.Username
                }), Encoding.UTF8, "application/json"));
            if (result.StatusCode == HttpStatusCode.Unauthorized)
                throw new UnauthorizedAccessException(await result.Content.ReadAsStringAsync());

            result.EnsureSuccessStatusCode();
        }

        public async Task Logout()
        {
            var result = await _httpClient.PostAsync("api/Authentication/Logout", null);
            result.EnsureSuccessStatusCode();
        }

        public async Task<CurrentUserModel> CurrentUserInfo()
        {
            var result = await _httpClient.GetAsync("api/Authentication/GetCurrentUserInfo");
            if (!result.IsSuccessStatusCode)
                return new AnonymousUser();

            var content = JsonConvert.DeserializeObject<CurrentUserResponse>(await result.Content.ReadAsStringAsync());
            return new CurrentUserModel
                {Claims = content.Claims, IsAuthenticated = content.IsAuthenticated, UserName = content.UserName};
        }
    }
}
