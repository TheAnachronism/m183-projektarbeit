﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M183Project.Common.Post
{
    public enum PostStatus
    {
        Hidden,
        Deleted,
        Published
    }
}
