﻿using System;
using System.Linq;
using System.Threading.Tasks;
using M183Project.Common.Roles;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace M183Project.Common
{
    public static class HostExtensions
    {
        public static async Task<IHost> MigrateDatabase<TContext>(this IHost webHost, ILogger<object> logger)
            where TContext : DbContext
        {
            var serviceScopeFactory = webHost.Services.GetService<IServiceScopeFactory>();

            using var scope = serviceScopeFactory.CreateScope();
            var services = scope.ServiceProvider;
            var dbContext = services.GetRequiredService<TContext>();

            var pendingMigrations = (await dbContext.Database.GetPendingMigrationsAsync()).ToList();
            if (pendingMigrations.Any())
            {
                logger.LogWarning($"Trying to apply {pendingMigrations.Count} pending migrations.");
                await dbContext.Database.MigrateAsync();
                logger.LogWarning($"Successfully applied {pendingMigrations.Count} pending database migrations.");
            }

            return webHost;
        }

        public static async Task<IHost> EnsureAdminUser<TUser>(this IHost webHost, ILogger<object> logger)
            where TUser : IdentityUser
        {
            var serviceScopeFactory = webHost.Services.GetService<IServiceScopeFactory>();

            using var scope = serviceScopeFactory!.CreateScope();
            var services = scope.ServiceProvider;
            var userManager = services.GetService<UserManager<TUser>>();
            var user = await userManager!.FindByNameAsync("Administrator");
            if (user == null)
            {
                Console.WriteLine("Please enter the phone number for the administrator user.");
                var phoneNumber = Console.ReadLine();
                user = Activator.CreateInstance<TUser>();
                user.UserName = "Administrator";
                user.Email = "Test@Test.com";
                user.PhoneNumber = phoneNumber;
                var result = await userManager.CreateAsync(user, "Administrator1234$");
                if (!result.Succeeded)
                    throw new ArgumentException(result.Errors.First().Description);

            }

            if (!await userManager.IsInRoleAsync(user, DatabaseRoles.Administrator))
                await userManager.AddToRoleAsync(user, DatabaseRoles.Administrator);

            return webHost;
        }
    }
}
