﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace M183Project.Common
{
    public static class SerilogConfigurationExtensions
    {
        public static LoggerConfiguration ConfigureDefaultLogging(this LoggerConfiguration loggerConfig, IConfiguration configuration, Assembly service)
        {
            loggerConfig.MinimumLevel.Debug();
            loggerConfig
                .MinimumLevel.Override("Default", LogEventLevel.Information)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information);

            loggerConfig.Enrich.FromLogContext().Enrich.WithMachineName();

            loggerConfig
                .WriteTo.Logger(logger =>
                {
                    logger.WriteTo.Console(outputTemplate: "{Timestamp:o} [{Level:u3}] ({SourceContext}) {Message}{NewLine}{Exception}");
                })
                .WriteTo.Logger(logger =>
                {
                    logger.Filter.ByIncludingOnly("(@l = 'Error' or @l = 'Fatal' or @l = 'Warning')");

                    var path = Path.Combine(AppContext.BaseDirectory, $"logs/{service.GetName().Name}/exceptions.log");
                    logger.WriteTo.File(
                        path,
                        outputTemplate:
                        "{Timestamp:o} [{Level:u3}] ({SourceContext}) {Message}{NewLine}{Exception}",
                        rollingInterval: RollingInterval.Day,
                        retainedFileCountLimit: 7);
                })
                .WriteTo.Logger(logger =>
                {
                    logger.Filter.ByIncludingOnly("(@l = 'Information' or @l = 'Debug')");

                    var path = Path.Combine(AppContext.BaseDirectory, $"logs/{service.GetName().Name}/information_.log");
                    logger.WriteTo.File(
                        path,
                        outputTemplate:
                        "{Timestamp:o} [{Level:u3}] ({SourceContext}) {Message}{NewLine}{Exception}",
                        rollingInterval: RollingInterval.Day,
                        retainedFileCountLimit: 7);
                });

            return loggerConfig;
        }

    }
}
