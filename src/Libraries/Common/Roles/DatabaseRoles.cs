﻿namespace M183Project.Common.Roles
{
    public static class DatabaseRoles
    {
        public const string Administrator = "Administrator";
        public const string User = "User";
    }
}
