﻿namespace M183Project.Shared
{
    public record GetVirtualizeRequest(int StartIndex, int PageSize = 10);
}
