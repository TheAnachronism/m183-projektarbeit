﻿namespace M183Project.Shared.User
{
    public record GetUserManageResponse(string UserName, string Role);

    public record GetUserResponse(string UserName);
}
