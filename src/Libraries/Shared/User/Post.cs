﻿namespace M183Project.Shared.User
{
    public record SetUserRoleRequest(string UserName, string NewRole);
}
