﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace M183Project.Shared.Authentication
{
    public record LoginRequest([Required] string UserName);

    public record SmsLoginRequest([Required] string UserName, [Required] string Password, [Required] string SmsCode);

    public record CurrentUserResponse(bool IsAuthenticated, string UserName, IEnumerable<KeyValuePair<string, string>> Claims);
}
