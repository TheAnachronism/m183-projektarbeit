﻿namespace M183Project.Shared.Manage
{
    public record VerifyChangePhoneNumberRequest(string PhoneNumber, string Token);
}
