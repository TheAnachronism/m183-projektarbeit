﻿namespace M183Project.Shared.Manage
{
    public record ChangePhoneNumberRequest(string PhoneNumber);
}
