﻿namespace M183Project.Shared.Manage
{
    public record TwoFactorResponse(bool HasAuthenticator, bool IsAuthenticatorEnabled, bool IsClientRemembered, int RecoveryCodesLeft);

    public record TwoFactorGenerateResponse(string[] RecoveryCodes);

    public record TwoFactorEnableRequest(string Code);

    public record TwoFactorEnableResponse(string[] RecoveryCodes, bool NewRecoveryCodesWereGenerated);

    public record TwoFactorInfoResponse(string SharedKey, string QrCodeUri);
}