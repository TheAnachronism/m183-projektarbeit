﻿namespace M183Project.Shared.Manage
{
    public record GetPhoneNumberInfoResult(string PhoneNumber);
}
