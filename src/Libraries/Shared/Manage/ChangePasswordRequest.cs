﻿namespace M183Project.Shared.Manage
{
    public record ChangePasswordRequest(string CurrentPassword, string NewPassword, string NewPasswordConfirm);
}
