﻿using System;

namespace M183Project.Shared.Post
{
    public record CreatePostRequest(string Title, string Content);

    public record DeletePostRequest(Guid Id);

    public record HidePostRequest(Guid Id);

    public record PublishPostRequest(Guid Id, string Code);
}
