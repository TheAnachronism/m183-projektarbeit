﻿using System;
using System.Collections.Generic;
using M183Project.Common.Post;

namespace M183Project.Shared.Post
{
    public record GetPostResponse(Guid Id, string Title, string Content, PostStatus Status, string OriginalPosterUsername, DateTime UtcCreatedAt, DateTime UtcLastEdited);
    public record GetVirtualizeResponse<T>(IEnumerable<T> Items, int TotalSize);
}
