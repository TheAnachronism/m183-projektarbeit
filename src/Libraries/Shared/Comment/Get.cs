﻿using System;

namespace M183Project.Shared.Comment
{
    public record GetCommentResponse(Guid PostId, string PostTitle, string OriginalPosterUsername, DateTime CreatedAt, string Content);
}
