﻿using System;

namespace M183Project.Shared.Comment
{
    public record PostCommentRequest(Guid PostId, string Content);
}
